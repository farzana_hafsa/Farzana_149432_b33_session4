<?php
echo substr_compare("Hello world","Hello world ",0,4);
echo substr_compare("bitm class","bitm",0,5 );
echo substr_compare("Hello world","world",5,12);
echo substr_compare("abcde", "cd", 1, 3); // 0
echo substr_compare("abcde", "de", -3); // 0
echo substr_compare("abcde", "bcg", 1, 2); // 0
echo substr_compare("WOMEN", "ME", 1, 4); // 0
echo substr_compare("abcde", "BC", 1, 2, true); // 0
echo substr_compare("abcde", "bc", 1, 3); // 1
echo substr_compare("abcde", "cd", 1, 2); // -1
echo substr_compare("abcde", "abc", 5, 1); // warning
?>
